

# Build

```
docker build -t daki/wazuh .
```

# Run

```
docker-compose up -d
```
