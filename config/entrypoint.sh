#!/bin/bash
# Wazuh Docker Copyright (C) 2020 Wazuh Inc. (License GPLv2)

#chown ossec:ossec /var/ossec/logs
chown ossec:ossec -R /var/ossec/logs/*
# It will run every .sh script located in entrypoint-scripts folder in lexicographical order
for script in `ls /entrypoint-scripts/*.sh | sort -n`; do
  bash "$script"

done

##############################################################################
# Start Wazuh Server.
##############################################################################

/sbin/my_init
